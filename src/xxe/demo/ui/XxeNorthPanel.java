package xxe.demo.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.stream.Stream;

import javax.swing.JComboBox;
import javax.swing.JLabel;

import xxe.demo.model.SecurityMeasures;
import xxe.demo.model.XxeAttack;
import xxe.demo.ui.config.JComboBoxConfig;
import xxe.demo.ui.config.JComponentConfig;
import xxe.demo.ui.config.JLabelConfig;

public class XxeNorthPanel extends AbstractXxePanel {

    private static final long serialVersionUID = 1L;

    @JComponentConfig(width = 200, height = 30)
    @JLabelConfig(text = "Choose XXE attack : ")
    private JLabel attackLabel = new JLabel();

    @JComponentConfig(width = 200, height = 30)
    @JLabelConfig(text = "Choose XXE attack : ")
    private JLabel securityLabel = new JLabel();

    @JComponentConfig(width = 600, height = 30, isOpaque = true)
    @JComboBoxConfig(allowsEmptySelection = true)
    private JComboBox<XxeAttack> attackPicker = new JComboBox<>();

    @JComponentConfig(width = 600, height = 30, isOpaque = true)
    @JComboBoxConfig(allowsEmptySelection = false)
    private JComboBox<SecurityMeasures> securityMeasuresPicker = new JComboBox<>();

    private final GridBagConstraints constraints = new GridBagConstraints();
    private static final Insets STANDARD_MARGINS = new Insets(5, 5, 5, 5);

    public XxeNorthPanel() {
	this.setLayout(new GridBagLayout());

	this.constraints.gridheight = 1;
	this.constraints.gridwidth = 1;
	this.constraints.insets = STANDARD_MARGINS;
	this.constraints.fill = GridBagConstraints.BOTH;

	initDropdownLabels();
	initDropdowns();
    }

    protected void initDropdownLabels() {
	this.constraints.gridx = 0;
	this.constraints.gridy = 0;
	this.constraints.weightx = 0.4;
	this.add(this.attackLabel, this.constraints);

	this.constraints.gridx = 0;
	this.constraints.gridy = 1;
	this.constraints.weightx = 0.4;
	this.add(this.securityLabel, this.constraints);
    }

    protected void initDropdowns() {
	Stream.of(XxeAttack.values()).forEach(this.attackPicker::addItem);
	allowNoSelectionInDropDown(this.attackPicker, true);

	constraints.gridx = 1;
	constraints.gridy = 0;
	constraints.weightx = 0.8;
	this.add(this.attackPicker, constraints);

	Stream.of(SecurityMeasures.values()).forEach(this.securityMeasuresPicker::addItem);
	allowNoSelectionInDropDown(this.securityMeasuresPicker, false);

	constraints.gridx = 1;
	constraints.gridy = 1;
	constraints.weightx = 0.8;
	this.add(this.securityMeasuresPicker, constraints);
    }

    private void allowNoSelectionInDropDown(final JComboBox<?> comboBox, final boolean noSelectionAllowed) {
	if (noSelectionAllowed) {
	    comboBox.addItem(null);
	    comboBox.getModel().setSelectedItem(null);
	}
    }
}
