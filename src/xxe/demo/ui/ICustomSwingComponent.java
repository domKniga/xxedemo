package xxe.demo.ui;

import java.awt.Font;
import java.awt.Insets;

public interface ICustomSwingComponent {

    Insets STANDARD_MARGINS = new Insets(5, 5, 5, 5);
    Font STANDARD_FONT = new Font("Rockwell", Font.BOLD, 18);

    // Component addCustomizedComponent(Component component);

    // default void processCommonConfig(Component component) {
    // try {
    // SwingAnnotationProcessor.doProcess((JComponent) component);
    // } catch (IllegalArgumentException | IllegalAccessException e) {
    // e.printStackTrace();
    // }
    // }

    // default <T> void processCommonConfig(Class<T> componentClass) {
    //
    // }
    //
    // default <T> void processComponentConfig(Class<T> componentClass) {
    //
    // }
}
