package xxe.demo.ui;

import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import xxe.demo.model.SecurityMeasures;
import xxe.demo.model.XxeAttack;
import xxe.demo.model.resources.ModelResourceLoader;
import xxe.demo.ui.config.JButtonConfig;
import xxe.demo.ui.config.JComponentConfig;

/**
 * Action toolbar UI component.
 * 
 * @author giliev
 */
public class XxeActionsToolBar extends JToolBar implements ICustomSwingComponent {

    private static final long serialVersionUID = 1L;

    private static final Insets COMPONENT_MARGINS = new Insets(5, 5, 5, 5);
    private static final Dimension COMPONENT_SEPARATOR = new Dimension(10, 10);

    private static final Dimension CONTAINER_SIZE = new Dimension(1000, 100);
    private static final Dimension COMPONENT_SIZE = new Dimension(330, 100);

    private static final String RUN_BTN_ICON = "runBtnIcon.png";
    private static final String REFRESH_BTN_ICON = "refreshBtnIcon.png";
    private static final String CLEAR_BTN_ICON = "clearBtnIcon.png";

    @JComponentConfig(width = 330, height = 100)
    @JButtonConfig(iconName = "runBtnIcon.png", isEnabled = false, isDefaultCapable = true)
    private JButton btnRunXxeAttack = new JButton();

    @JComponentConfig(width = 330, height = 100)
    @JButtonConfig(iconName = "refreshBtnIcon.png", fillContentArea = false)
    private JButton btnRefreshRequest = new JButton();

    @JComponentConfig(width = 330, height = 100)
    @JButtonConfig(iconName = "clearBtnIcon.png")
    private JButton btnClearView = new JButton();

    public XxeActionsToolBar() {
	this.setOpaque(false);
	this.setMargin(COMPONENT_MARGINS);
	this.setOrientation(SwingConstants.HORIZONTAL);
	this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
	this.setPreferredSize(CONTAINER_SIZE);

	this.setOpaque(false);
	this.setRollover(true);
	this.setFloatable(false);
	this.setAutoscrolls(false);
	this.setEnabled(true);
	this.setFocusable(true);
	this.setRequestFocusEnabled(true);
	this.setVisible(true);

	setupButtons();
    }

    private void setupButtons() {
	// this.btnRunXxeAttack.setVisible(true);
	this.btnRunXxeAttack.setEnabled(false);
	this.btnRunXxeAttack.setFocusable(true);
	this.btnRunXxeAttack.setContentAreaFilled(true);
	this.btnRunXxeAttack.setDefaultCapable(true);
	this.btnRunXxeAttack.setRequestFocusEnabled(true);
	this.btnRunXxeAttack.setRolloverEnabled(true);

	this.btnRunXxeAttack.setIcon(new ImageIcon(ModelResourceLoader.class.getResource(RUN_BTN_ICON)));
	this.btnRunXxeAttack.setMargin(COMPONENT_MARGINS);
	this.btnRunXxeAttack.setPreferredSize(COMPONENT_SIZE);

	this.btnRefreshRequest.setVisible(true);
	this.btnRefreshRequest.setEnabled(true);
	this.btnRefreshRequest.setFocusable(true);
	this.btnRefreshRequest.setContentAreaFilled(true);
	this.btnRefreshRequest.setDefaultCapable(true);
	this.btnRefreshRequest.setRequestFocusEnabled(true);
	this.btnRefreshRequest.setRolloverEnabled(true);

	// TODO:
	this.btnRefreshRequest.setIcon(new ImageIcon(ModelResourceLoader.class.getResource(REFRESH_BTN_ICON)));
	this.btnRefreshRequest.setMargin(COMPONENT_MARGINS);
	this.btnRefreshRequest.setPreferredSize(COMPONENT_SIZE);

	this.btnClearView.setVisible(true);
	this.btnClearView.setEnabled(true);
	this.btnClearView.setFocusable(true);
	this.btnClearView.setContentAreaFilled(true);
	this.btnClearView.setDefaultCapable(true);
	this.btnClearView.setRequestFocusEnabled(true);
	this.btnClearView.setRolloverEnabled(true);

	// TODO:
	this.btnClearView.setIcon(new ImageIcon(ModelResourceLoader.class.getResource(CLEAR_BTN_ICON)));
	this.btnClearView.setMargin(COMPONENT_MARGINS);
	this.btnClearView.setPreferredSize(COMPONENT_SIZE);

	// TODO;
	// processCommonConfig(this);

	this.add(btnClearView);
	this.addSeparator(COMPONENT_SEPARATOR);
	this.add(btnRefreshRequest);
	this.addSeparator(COMPONENT_SEPARATOR);
	this.add(btnRunXxeAttack);
    }

    // @Override
    // public Component addCustomizedComponent(Component component) {
    // final JButton btn = (JButton) super.add(component);
    //
    // return null;
    // }

    // protected JButton addButton(Component component){
    //
    // return component;
    // }

    public void addRunAttackAction(final ActionListener action) {
	this.btnRunXxeAttack.addActionListener(action);
    }

    public void addResetViewAction(final ActionListener action) {
	this.btnClearView.addActionListener(action);
    }

    public void addRefreshRequestAction(final ActionListener action) {
	this.btnRefreshRequest.addActionListener(action);
    }

    public void toggleRunButton(final boolean enable) {
	this.btnRunXxeAttack.setEnabled(enable);
    }

    public void runAttack(final XxeAttack attack, final SecurityMeasures measures) {
	// TODO:...
	SwingUtilities.invokeLater(new Thread());
    }

}
