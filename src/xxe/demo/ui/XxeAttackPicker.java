package xxe.demo.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.util.stream.Stream;

import javax.swing.JComboBox;

import xxe.demo.model.XxeAttack;

/**
 * Picker for XXE attacks UI component.
 * 
 * @author giliev
 */
public class XxeAttackPicker extends JComboBox<XxeAttack>{

    private static final long serialVersionUID = 1L;

    private static final Dimension COMPONENT_SIZE = new Dimension(600, 30);
    private static final Font STANDARD_FONT = new Font("Rockwell", Font.BOLD, 18);

    public XxeAttackPicker() {
	this.setOpaque(false);
	this.setVisible(true);
	this.setFont(STANDARD_FONT);
	this.setPreferredSize(COMPONENT_SIZE);

	Stream.of(XxeAttack.values()).forEach(this::addItem);
	includeEmptySelection();
    }

    private void includeEmptySelection() {
	this.addItem(null);
	this.getModel().setSelectedItem(null);
    }
}
