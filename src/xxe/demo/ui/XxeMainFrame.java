package xxe.demo.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import xxe.demo.XxeRunner;
import xxe.demo.XxeRunner.XxeResponse;
import xxe.demo.model.SecurityMeasures;
import xxe.demo.model.XxeAttack;
import xxe.demo.ui.config.JComponentConfig;
import xxe.demo.ui.config.JPanelConfig;

public class XxeMainFrame extends AbstractXxeFrame {

    private static final String FRAME_TITLE_DEFAULT = "N/A";
    private static final Font STANDARD_FONT = new Font("Rockwell", Font.BOLD, 18);
    private static final int STANDARD_CONTAINER_WIDTH = 1000;
    private static final int STANDARD_LABEL_WIDTH = 200;

    private static final int STANDARD_LABEL_HEIGHT = 30;

    private static final double STANDARD_LABEL_RATIO = 0.4d;
    private static final double STANDARD_PICKER_RATIO = 0.6d;

    private static final Insets STANDARD_MARGINS = new Insets(5, 5, 5, 5);

    private static final String FRAME_TITLE_FORMAT = "XXE attack demo :: [ %s ]";

    private static final long serialVersionUID = 1L;

    private final XxeAttackPicker attackPicker = new XxeAttackPicker();
    private final XxeSecurityMeasuresPicker sequrityMeasuresPicker = new XxeSecurityMeasuresPicker();

    private final XxeResponseSplitView requestResponseView = new XxeResponseSplitView();

    private final XxeActionsToolBar actionsToolbar = new XxeActionsToolBar();

    @JComponentConfig(width = 1000, height = 100)
    @JPanelConfig(toolTip = "Panel containing configuration drop-downs...")
    private XxeNorthPanel northPanel = new XxeNorthPanel();

    private XxeResponse actionResponse = null;

    public XxeMainFrame() {
	// doProcess(this);

	// setAutoRequestFocus(true);
	// setAlwaysOnTop(true);
	setResizable(true);
	// setFocusable(true);
	// setEnabled(true);

	// TODO: Append title when running attack
	setTitle(String.format(FRAME_TITLE_FORMAT, FRAME_TITLE_DEFAULT));
	setFont(STANDARD_FONT);
	// setLayout(new GridBagLayout());
	// setPreferredSize(new Dimension(1000, 700));
	setSize(new Dimension(STANDARD_CONTAINER_WIDTH, 800));
	setLocationRelativeTo(null);
	setDefaultCloseOperation(EXIT_ON_CLOSE);

	getContentPane().setFont(STANDARD_FONT);
	getContentPane().setBackground(Color.BLACK);
	getContentPane().setLayout(new BorderLayout());
    }

    public void sh() {
	final GridBagConstraints constraints = new GridBagConstraints();

	final JPanel northPanel = new JPanel();
	northPanel.setOpaque(false);
	northPanel.setLayout(new GridBagLayout());
	northPanel.setPreferredSize(new Dimension(STANDARD_CONTAINER_WIDTH, 100));

	constraints.gridheight = 1;
	constraints.gridwidth = 1;
	constraints.insets = STANDARD_MARGINS;
	constraints.fill = GridBagConstraints.BOTH;
	// constraints.anchor = GridBagConstraints.NORTH;

	final JLabel attackLabel = new JLabel("Choose XXE attack : ");
	attackLabel.setOpaque(false);
	// attackLabel.setHorizontalTextPosition(SwingConstants.CENTER);
	attackLabel.setPreferredSize(new Dimension(STANDARD_LABEL_WIDTH, STANDARD_LABEL_HEIGHT));
	attackLabel.setForeground(Color.WHITE);
	attackLabel.setFont(STANDARD_FONT);

	// attackLabel.set
	constraints.gridx = 0;
	constraints.gridy = 0;
	constraints.weightx = STANDARD_LABEL_RATIO;
	// constraints.fill = GridBagConstraints.HORIZONTAL;
	// constraints.anchor = GridBagConstraints.EAST;
	northPanel.add(attackLabel, constraints);

	constraints.gridx = 1;
	constraints.gridy = 0;
	constraints.weightx = STANDARD_PICKER_RATIO;
	// constraints.anchor = GridBagConstraints.WEST;
	northPanel.add(attackPicker, constraints);

	final JLabel securityLabel = new JLabel("Choose security measures :");
	securityLabel.setOpaque(false);
	securityLabel.setPreferredSize(new Dimension(STANDARD_LABEL_WIDTH, STANDARD_LABEL_HEIGHT));
	securityLabel.setForeground(Color.WHITE);
	securityLabel.setFont(STANDARD_FONT);

	constraints.gridx = 0;
	constraints.gridy = 1;
	constraints.weightx = STANDARD_LABEL_RATIO;
	// constraints.anchor = GridBagConstraints.EAST;
	northPanel.add(securityLabel, constraints);

	constraints.gridx = 1;
	constraints.gridy = 1;
	constraints.weightx = STANDARD_PICKER_RATIO;
	northPanel.add(sequrityMeasuresPicker, constraints);

	getContentPane().add(northPanel, BorderLayout.NORTH);
	// this.add(northPanel, BorderLayout.NORTH);

	final JPanel middlePanel = new JPanel();
	middlePanel.setOpaque(false);
	middlePanel.setLayout(new GridBagLayout());
	middlePanel.setPreferredSize(new Dimension(STANDARD_CONTAINER_WIDTH, 500));

	constraints.gridx = 0;
	constraints.gridy = 0;
	constraints.weightx = 1;
	constraints.weighty = 1;
	// constraints.fill = GridBagConstraints.BOTH;
	// constraints.anchor = GridBagConstraints.CENTER;
	middlePanel.add(requestResponseView, constraints);

	getContentPane().add(middlePanel, BorderLayout.CENTER);

	final JPanel southPanel = new JPanel();
	southPanel.setOpaque(false);
	southPanel.setLayout(new GridBagLayout());
	southPanel.setPreferredSize(new Dimension(STANDARD_CONTAINER_WIDTH, 100));

	// constraints.anchor = GridBagConstraints.EAST;
	// constraints.fill = GridBagConstraints.HORIZONTAL;
	southPanel.add(actionsToolbar, constraints);

	getContentPane().add(southPanel, BorderLayout.SOUTH);
    }

    public void func() {
	attackPicker.addItemListener(new ItemListener() {
	    @Override
	    public void itemStateChanged(ItemEvent e) {
		if (e.getStateChange() == ItemEvent.SELECTED) {
		    setTitle(String.format(FRAME_TITLE_FORMAT, e.getItem()));
		    actionsToolbar.toggleRunButton(true);
		} else {
		    setTitle(String.format(FRAME_TITLE_FORMAT, FRAME_TITLE_DEFAULT));
		    actionsToolbar.toggleRunButton(false);
		}
	    }
	});

	actionsToolbar.addRunAttackAction(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		final XxeResponse response = XxeRunner.runXxeAttack(((XxeAttack) attackPicker.getSelectedItem()),
			(SecurityMeasures) sequrityMeasuresPicker.getSelectedItem());

		try {
		    requestResponseView.displayResponse(response.getResponseCode(), response.getResponseMessage(),
			    response.getResponseXml());
		} catch (Exception e2) {
		    actionResponse = new XxeResponse("ERROR", e2.getMessage(), "");

		    requestResponseView.displayResponse(actionResponse.getResponseCode(),
			    actionResponse.getResponseMessage(), actionResponse.getResponseXml());
		}

	    }
	});

	actionsToolbar.addResetViewAction(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		requestResponseView.clear();
		attackPicker.setSelectedItem(null);

		sequrityMeasuresPicker.setSelectedItem(SecurityMeasures.NONE);
		requestResponseView.previewSecurityMeasures(SecurityMeasures.NONE);
	    }
	});

	sequrityMeasuresPicker.addItemListener(new ItemListener() {

	    @Override
	    public void itemStateChanged(ItemEvent e) {
		requestResponseView.previewSecurityMeasures((SecurityMeasures) e.getItem());
	    }
	});
    }
}