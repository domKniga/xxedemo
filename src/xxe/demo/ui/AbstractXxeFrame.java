package xxe.demo.ui;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JFrame;

import xxe.demo.ui.config.SwingAnnotationProcessor;

public class AbstractXxeFrame extends JFrame {

    private static final long serialVersionUID = 1L;

    public AbstractXxeFrame() {
    }

    @Override
    public Component add(Component comp) {
	doProcess(comp);
	return super.add(comp);
    }

    @Override
    public void add(Component comp, Object constraints) {
	doProcess(comp);
	super.add(comp, constraints);
    }

    protected void doProcess(Component component) {
	try {
	    SwingAnnotationProcessor.doProcess(component instanceof JComponent ? (JComponent) component : component);
	} catch (IllegalArgumentException | IllegalAccessException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }
}
