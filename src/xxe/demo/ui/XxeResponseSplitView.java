package xxe.demo.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Insets;
import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import xxe.demo.model.SecurityMeasures;

/**
 * Split view for response console/content of XXE attacks.
 * 
 * @author giliev
 */
public class XxeResponseSplitView extends JSplitPane {

    private static final long serialVersionUID = 1L;

    /**
     * Split ratio 80/20 for components.
     */
    private static final double COMPONENT_SPLIT_RATIO = 0.8;
    private static final Font COMPONENT_FONT = new Font("Rockwell", Font.BOLD, 18);
    private static final Insets COMPONENT_MARGINS = new Insets(5, 5, 5, 5);

    private final JEditorPane paneResponseConsole = new JEditorPane();
    private final JEditorPane paneResponseContent = new JEditorPane();

    public XxeResponseSplitView() {
	this.setOrientation(JSplitPane.VERTICAL_SPLIT);

	// Response console
	setAsXmlEditor(paneResponseConsole, false);
	styleAsEditor(paneResponseConsole);
	previewSecurityMeasures(SecurityMeasures.NONE);

	// Response content
	setAsXmlEditor(paneResponseContent, false);
	styleAsEditor(paneResponseContent);

	this.setBottomComponent(makeScrollable(paneResponseConsole));
	this.setTopComponent(makeScrollable(paneResponseContent));
	this.setResizeWeight(COMPONENT_SPLIT_RATIO);
    }

    /**
     * Apply common styling to editorPane
     * 
     * @param editPane
     *            - the editor pane to style.
     */
    protected void styleAsEditor(final JEditorPane editPane) {
	editPane.setBackground(Color.DARK_GRAY);
	editPane.setForeground(Color.WHITE);

	editPane.setFont(COMPONENT_FONT); // common
	editPane.setMargin(COMPONENT_MARGINS); // common

	editPane.setAutoscrolls(true); // common
	editPane.setEnabled(true); // common
	editPane.setFocusable(true); // common
	editPane.setVisible(true); // common
    }

    /**
     * Sets up a specified editor pane as XML viewer/editor.
     * 
     * @param editPane
     *            - the editor pane to setup.
     * @param isEditable
     *            - whether XML should be editable or not.
     */
    protected void setAsXmlEditor(final JEditorPane editPane, final boolean isEditable) {
	editPane.setEditable(isEditable);

	final String contentTypeXml = "text/xml";
	final String contentTypeHtml = "text/html";

	editPane.setContentType(contentTypeXml);
	editPane.setEditorKitForContentType(contentTypeXml, JEditorPane.createEditorKitForContentType(contentTypeHtml));
    }

    /**
     * Previews the security measures in the console editor pane.
     */
    public void previewSecurityMeasures(final SecurityMeasures measures) {
	final String securityMeasuresHeader = "[SaxParser] :: Measures = " + measures.toString();
	displayConsoleOutput(securityMeasuresHeader
		+ "\n---------------------------------------------------------------------------------------------------------------------------------------------------------------\n"
		+ measures.getPreview());
    }

    private JScrollPane makeScrollable(final JEditorPane editPane) {
	return new JScrollPane(editPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
		JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    /**
     * Clears all display components in the container.
     */
    public void clear() {
	clearEditorsRecursively(this);
    }

    private void clearEditorsRecursively(final Component component) {
	if (component instanceof JEditorPane) { // clear editor pane
	    ((JEditorPane) component).setText(null);
	    ((JEditorPane) component).setCaretPosition(0);
	}

	if (component instanceof Container) {
	    for (Component child : ((Container) component).getComponents()) {
		clearEditorsRecursively(child);
	    }
	}
    }

    /**
     * @param responseCode
     *            - the output response code.
     * @param responseMsg
     *            - the output response message.
     */
    public void displayConsoleOutput(String responseCode, String responseMsg) {
	paneResponseConsole.setForeground("OK".equals(responseCode) ? Color.GREEN : Color.RED);
	paneResponseConsole.setText(responseCode + "\n" + responseMsg);
    }

    /**
     * @param responseCode
     *            - the response code.
     * @param responseMsg
     *            - the response message.
     * @param responseXml
     *            - the response content
     */
    public void displayResponse(String responseCode, String responseMsg, String responseXml) {
	paneResponseContent.setText(XmlPrinter.toPrettyString(responseXml));
	paneResponseContent.setCaretPosition(0);

	displayConsoleOutput(responseCode, responseMsg);
    }

    // TODO: switch to display in new editor when you impl it...
    public void displayConsoleOutput(String securityMeasuresPreviewText) {
	paneResponseConsole.setForeground(Color.WHITE);
	paneResponseConsole.setText(securityMeasuresPreviewText);
	paneResponseConsole.setCaretPosition(0);
    }
    //

    private static class XmlPrinter {

	/**
	 * @param xmlContent
	 *            - the XML content to print
	 * @return A properly indented and visually formatted XML output.
	 */
	public static String toPrettyString(String xmlContent) {
	    if ("".equals(xmlContent)) {
		return "";
	    }

	    try {
		final String charsetName = StandardCharsets.UTF_8.name();

		// Turn xml string into a document
		final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		// dbf.setXIncludeAware(false);
		// dbf.setExpandEntityReferences(false);
		// dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl",
		// true);
		dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
		// dbf.setFeature("http://xml.org/sax/features/external-parameter-entities",
		// false);
		// dbf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd",
		// false);

		Document document = dbf.newDocumentBuilder()
			.parse(new InputSource(new ByteArrayInputStream(xmlContent.getBytes(charsetName))));

		// Remove whitespaces outside tags
		document.normalize();
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']", document,
			XPathConstants.NODESET);

		for (int i = 0; i < nodeList.getLength(); ++i) {
		    Node node = nodeList.item(i);
		    node.getParentNode().removeChild(node);
		}

		// Setup pretty print options
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setAttribute("indent-number", 4);

		Transformer transformer = transformerFactory.newTransformer();
		// transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
		// "4");

		transformer.setOutputProperty(OutputKeys.ENCODING, charsetName);
		// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
		// "yes");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		// Return pretty print xml string
		StringWriter stringWriter = new StringWriter();
		transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
		return stringWriter.toString();
	    } catch (Exception e) {
		throw new RuntimeException(e);
	    }
	}
    }
}
