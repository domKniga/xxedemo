package xxe.demo.ui;

import java.awt.Component;

import javax.swing.JComponent;
import javax.swing.JPanel;

import xxe.demo.ui.config.SwingAnnotationProcessor;

public abstract class AbstractXxePanel extends JPanel {

    private static final long serialVersionUID = 1L;

    public AbstractXxePanel() {
    }

    @Override
    public Component add(Component comp) {
	doProcess(comp);
	return super.add(comp);
    }

    @Override
    public void add(Component comp, Object constraints) {
	doProcess(comp);
	super.add(comp, constraints);
    }

    protected void doProcess(Component component) {
	try {
	    SwingAnnotationProcessor.doProcess((JComponent) component);
	} catch (IllegalArgumentException | IllegalAccessException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }
}
