package xxe.demo.ui;

import java.awt.Dimension;
import java.awt.Font;
import java.util.stream.Stream;

import javax.swing.JComboBox;

import xxe.demo.model.SecurityMeasures;

/**
 * Picker for XXE security measures UI component.
 * 
 * @author giliev
 */
public class XxeSecurityMeasuresPicker extends JComboBox<SecurityMeasures> {

    private static final long serialVersionUID = 1L;

    private static final Dimension COMPONENT_SIZE = new Dimension(600, 30);
    private static final Font STANDARD_FONT = new Font("Rockwell", Font.BOLD, 18);

    public XxeSecurityMeasuresPicker() {
	this.setOpaque(false);
	this.setVisible(true);
	this.setFont(STANDARD_FONT);
	this.setPreferredSize(COMPONENT_SIZE);

	Stream.of(SecurityMeasures.values()).forEach(this::addItem);
	this.getModel().setSelectedItem(SecurityMeasures.NONE);
    }
}
