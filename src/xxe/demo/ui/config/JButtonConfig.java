package xxe.demo.ui.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JButtonConfig {
    boolean isEnabled() default true;
    boolean isDefaultCapable() default false;
    boolean fillContentArea() default true;
    String iconName();
}
