package xxe.demo.ui.config;

import java.awt.Font;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface JComponentConfig {
    boolean isOpaque() default false;

    boolean isVisible() default true;

    int width();

    int height();

    String foregroundColor() default "0X000000";

    String backgroundColor() default "0XFFFFFF";

    String fontFamily() default "Rockwell";

    int fontSize() default 18;

    int fontStyle() default Font.BOLD;
}
