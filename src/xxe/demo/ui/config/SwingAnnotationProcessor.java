package xxe.demo.ui.config;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import javax.swing.JComponent;

public class SwingAnnotationProcessor {

    static void processCommonConfig(JComponent classLevelComponent)
	    throws IllegalArgumentException, IllegalAccessException {
	final Field[] fields = classLevelComponent.getClass().getDeclaredFields();
	for (Field field : fields) {
	    if (field.getModifiers() != Modifier.PRIVATE) {
		continue;
	    }

	    field.setAccessible(true);
	    if (field.isAnnotationPresent(JComponentConfig.class)) {
		final JComponentConfig annotationConfig = field.getAnnotation(JComponentConfig.class);

		final JComponent fieldComponent = (JComponent) field.get(classLevelComponent);
		fieldComponent.setVisible(annotationConfig.isVisible());
		fieldComponent.setPreferredSize(new Dimension(annotationConfig.width(), annotationConfig.height()));
	    }
	}
    }

    static void processCommonConfig(Component classLevelComponent)
	    throws IllegalArgumentException, IllegalAccessException {
	final Field[] fields = classLevelComponent.getClass().getDeclaredFields();
	for (Field field : fields) {
	    if (field.getModifiers() != Modifier.PRIVATE) {
		continue;
	    }

	    field.setAccessible(true);
	    if (field.isAnnotationPresent(JComponentConfig.class)) {
		final JComponentConfig annotationConfig = field.getAnnotation(JComponentConfig.class);

		final JComponent fieldComponent = (JComponent) field.get(classLevelComponent);
		fieldComponent.setOpaque(annotationConfig.isOpaque());
		fieldComponent.setVisible(annotationConfig.isVisible());
		fieldComponent.setPreferredSize(new Dimension(annotationConfig.width(), annotationConfig.height()));
		fieldComponent.setFont(new Font(annotationConfig.fontFamily(), annotationConfig.fontStyle(),
			annotationConfig.fontSize()));
		fieldComponent.setBackground(Color.decode(annotationConfig.backgroundColor()));
		fieldComponent.setForeground(Color.decode(annotationConfig.foregroundColor()));
	    }
	}
    }

    // static void processComponentConfig(Component component) {
    // final Class<? extends Component> componentClass = component.getClass();
    // if (componentClass.isAssignableFrom(JButton.class)) {
    // pp(componentClass, annotationClass)
    //
    // }
    // }
    //
    // private static <T> Annotation pp(Class<T> componentClass, Class<? extends
    // Annotation> annotationClass) {
    // final Field[] fields = componentClass.getClass().getDeclaredFields();
    // for (Field field : fields) {
    // if (field.isAnnotationPresent(annotationClass)) {
    // return field.getAnnotation(annotationClass);
    // }
    // }
    //
    // throw new RuntimeException("FFS");
    // }
    //
    // private static <T> Annotation pr(Component component, Class<? extends
    // Annotation> annotationClass) {
    // final Field[] fields = component.getClass().getDeclaredFields();
    // for (Field field : fields) {
    // if (field.isAnnotationPresent(annotationClass)) {
    // field.getAnnotation(annotationClass);
    // }
    // }
    // S
    // throw new RuntimeException("FFS");
    // }

    public static void doProcess(JComponent classLevelComponent)
	    throws IllegalArgumentException, IllegalAccessException {
	processCommonConfig(classLevelComponent);
    }

    public static void doProcess(Component classLevelComponent)
	    throws IllegalArgumentException, IllegalAccessException {
	processCommonConfig(classLevelComponent);
    }
}
