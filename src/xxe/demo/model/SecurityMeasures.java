package xxe.demo.model;

public enum SecurityMeasures {
    NONE("factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);"), // nl
    HARD("factory.setFeature(\"http://apache.org/xml/features/disallow-doctype-decl\", true);\n"
	    + "factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);"), // nl
    MEDIUM("parser.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, Boolean.toString(Boolean.FALSE));"), // nl
    SOFT("parser.getXMLReader().setFeature(\"http://xml.org/sax/features/external-general-entities\", false);\n"
	    + "parser.getXMLReader().setFeature(\"http://xml.org/sax/features/external-parameter-entities\", false);\n"
	    + "parser.getXMLReader().setFeature(\"http://apache.org/xml/features/nonvalidating/load-external-dtd\",false);"), // nl
    JVM("factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);");

    private String preview;

    private SecurityMeasures(String preview) {
	this.preview = preview;
    }

    public String getPreview() {
	return preview;
    }
}
