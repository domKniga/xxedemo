package xxe.demo.model;

public enum XxeAttack {
    PrivateResource("Private resource attack", "privateResourceAttack.xml"), // nl
    MissingResource("Missing resouce DoS attack", "missingResourceAttack.xml"), // nl
    IllegalCharacterInjection("Illegal character injection DoS attack", "illegalCharacterAttack.xml"), // nl
    FtpResource("FTP resource attack", "ftpResourceAttack.xml"), // nl
    RemoteExecution("Remote execution attack", "remoteExecutionAttack.xml"), // nl
    PortScanAttack("Port scan DoS attack", "portScanAttack.xml"), // nl
    ExponentialBomb("Exponential XML bomb(Bilion laughs) DoS attack", "exponentialBombAttack.xml"), // nl
    QuadraticBomb("Quadratic XML bomb DoS attack", "quadraticBombAttack.xml");

    private String displayName;
    private String xmlFile;

    private XxeAttack(String displayName, String xmlFile) {
	this.displayName = displayName;
	this.xmlFile = xmlFile;
    }

    public String getXmlFile() {
	return this.xmlFile;
    }

    @Override
    public String toString() {
	return this.displayName;
    }
}
