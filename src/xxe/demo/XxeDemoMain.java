package xxe.demo;

import xxe.demo.ui.XxeMainFrame;

public final class XxeDemoMain {

    public static void main(String[] args) {
	final XxeMainFrame ex = new XxeMainFrame();
	ex.sh();
	ex.func();
	ex.pack();
	ex.setVisible(true);
    }
}
